﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium;
using System.Drawing;

namespace UITests
{
    class ChromeGossipTests
    {
        public static bool TestLoad( string webPage)
        {
            bool result = false;
            using (IWebDriver browser = new ChromeDriver())
            {
                result = GossipTests.TestLoad(browser, webPage);
                browser.Quit();
            }
            return result;
        }

        public static bool TestTitleLoad(string webPage, string webPageTitle)
        {
            bool result = false;
            using (IWebDriver browser = new ChromeDriver())
            {
                result = GossipTests.TestTitleLoad(browser, webPage, webPageTitle);
                browser.Quit();
            }
            return result;
        }

        public static bool TestHeaderTextLoad(string webPage, string webPageTitleImage)
        {
            bool result = false;
            using (IWebDriver browser = new ChromeDriver())
            {
                result = GossipTests.TestHeaderTextLoad(browser, webPage, webPageTitleImage);
                browser.Quit();
            }
            return result;
        }

        public static bool TestMenuColorsLoad(string webPage, Color color, Color bgcolor, out string message)
        {
            bool result = false;
            using (IWebDriver browser = new ChromeDriver())
            {
                result = GossipTests.TestMenuColorsLoad(browser, webPage, color, bgcolor, out message);
                browser.Quit();
            }
            return result;
        }

    }
}
