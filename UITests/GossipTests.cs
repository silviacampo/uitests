﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using System.Threading;
using System.Drawing;




//    IWebElement myfieldName = browser.FindElement(By.Id("loginName"));
//    myfieldName.Clear();
//    myfieldName.SendKeys("ff");

//    IWebElement myfieldPassword = browser.FindElement(By.Id("password"));
//    myfieldPassword.Clear();
//    myfieldPassword.SendKeys("pp");

//    IWebElement myfieldSubmit = browser.FindElement(By.Id("submitLogin"));
//    myfieldSubmit.Submit();




namespace UITests
{
    class GossipTests
    {
        public static bool TestLoad(IWebDriver browser, string webPage)
        {
            browser.Navigate().GoToUrl(webPage);
           return browser.Url.ToString().Equals(webPage);
        }
 
        public static bool TestTitleLoad(IWebDriver browser, string webPage, string webPageTitle)
        {
            browser.Navigate().GoToUrl(webPage);
            return browser.Title.Contains(webPageTitle);
        }

        public static bool TestHeaderTextLoad(IWebDriver browser, string webPage, string webPageTitleImage)
        {
            browser.Navigate().GoToUrl(webPage);
            IWebElement titleImage = browser.FindElement(By.Id(GossipLayout.gossipTitleImg));
            string GossipTitleImage = titleImage.GetCssValue("background-image");
                return GossipTitleImage.ToLower().Contains(webPageTitleImage.ToLower());
        }

        public static bool TestMenuColorsLoad(IWebDriver browser, string webPage, Color color, Color bcolor, out string message)
        {
                browser.Navigate().GoToUrl(webPage);
                IWebElement leftmenuDiv = browser.FindElement(By.Id(GossipLayout.leftMenuDiv));
                string BGColor = leftmenuDiv.GetCssValue("background-color");
                IWebElement leftmenuLink = leftmenuDiv.FindElements(By.TagName("a"))[0];
                string Color = leftmenuLink.GetCssValue("color");
            string paramcolor = RgbConverter(color);
            string parambcolor = RgbConverter(bcolor);
            message = "The color " +  Color + " is equal to " + paramcolor + " and the backgroundColor " + BGColor + " is equals to " + parambcolor;
                return equalsRGBA(Color,paramcolor) && equalsRGBA(BGColor, parambcolor);
        }

        private static string RgbConverter(Color c)
        {
            return String.Format("rgba({0},{1},{2},{3})", c.R, c.G, c.B, c.A);
        }

        private static bool equalsRGBA(string rgba1, string rgba2)
        {
            char[] parentesis = { '(', ')' };
            rgba1 = rgba1.Remove(0,4).Trim(parentesis);
            string[] arrayrgba1 = rgba1.Split(',');

            rgba2 = rgba2.Remove(0,4).Trim(parentesis);
            string[] arrayrgba2 = rgba2.Split(',');

            try
            {
                if ((float.Parse(arrayrgba1[0]) == float.Parse(arrayrgba2[0])) &&
                    (float.Parse(arrayrgba1[1]) == float.Parse(arrayrgba2[1])) &&
                    (float.Parse(arrayrgba1[2]) == float.Parse(arrayrgba2[2])) &&
                    (float.Parse(arrayrgba1[3]) == float.Parse(arrayrgba2[3])))
                {
                    return true;
                }
                else { return false; }
            }
            catch {
                return false;
            }

        }



        public static bool TestEmptyLogin(IWebDriver browser, string webPage, out string message)
        {
            bool result = true;
            message = "";

                browser.Navigate().GoToUrl(webPage);
                browser.FindElement(By.ClassName("formbutton")).Submit();
                if (browser.FindElement(By.ClassName("ErrorMessage")) == null)
                {
                    message = "Error message not displayed";
                    return false;
                }


                browser.Navigate().GoToUrl(webPage);
                browser.FindElement(By.XPath("//div[@id='" + GossipLogin.ForgotPasswordAjaxMessageDiv + "']/following-sibling::button[last()]")).Submit();
                Thread.Sleep(1000);
                if (browser.FindElement(By.Id(GossipLogin.ForgotPasswordAjaxMessageDiv)).GetAttribute("innerHTML") == String.Empty)
                {
                    message = "Ajax error message not displayed";
                    return false;
                }

            return result;
        }

        public static bool TestInvalidLogin(IWebDriver browser, string webPage, out string message)
        {
            bool result = true;
            message = "";
            browser.Navigate().GoToUrl(webPage);
                //browser.TextField(Find.ById(Login.UsernameInputText)).TypeText("12");
                //browser.TextField(Find.ById(Login.PasswordInputText)).TypeText("3456");
                //browser.Button(Find.ByClass("formbutton")).Click();
                //if (browser.Child(Find.ByClass("ErrorMessage")) == null)
                //{
                //    message = "Error message not displayed";
                //    return false;
                //}


                //browser.Navigate().GoToUrl(webPage);
                //browser.TextField(Find.ById(Login.ForgotPasswordEmailInputText)).TypeText("123456");
                //browser.Div(Find.ById("ajaxMessage")).NextSibling.NextSibling.Click();
                //Thread.Sleep(1000);
                //if (browser.Div(Find.ById("ajaxMessage")).InnerHtml == null)
                //{
                //    message = "Ajax error message not displayed";
                //    return false;
                //}

            return result;
        }

        public static bool TestInexistantLogin(IWebDriver browser, string webPage, out string message)
        {
            bool result = true;
            message = "";
            browser.Navigate().GoToUrl(webPage);
                //browser.TextField(Find.ById(Login.UsernameInputText)).TypeText("sssssss");
                //browser.TextField(Find.ById(Login.PasswordInputText)).TypeText("ppppppppp");
                //browser.Button(Find.ByClass("formbutton")).Click();
                //if (browser.Child(Find.ByClass("ErrorMessage")) == null)
                //{
                //    message = "Error message not displayed";
                //    return false;
                //}


                //browser.Navigate().GoToUrl(webPage);
                //browser.TextField(Find.ById(Login.ForgotPasswordEmailInputText)).TypeText("eeee@eeeeee.com");
                //browser.Div(Find.ById("ajaxMessage")).NextSibling.NextSibling.Click();
                //Thread.Sleep(1000);
                //if (browser.Div(Find.ById("ajaxMessage")).InnerHtml == null)
                //{
                //    message = "Ajax error message not displayed";
                //    return false;
                //}

            return result;
        }


    
    }
}
