﻿using System;
using System.Drawing;
using System.Text.RegularExpressions;

namespace UITests
{
    class GossipLayout 
    {
        public static string gossipTitleDiv = "GossipTitle"; 
        public static string gossipTitleImg = "GossipImg";
        public static string GSTitleImg = "http://www.gossipspecials.com/img/GossipSpecialsBadaboom.jpeg";
        public static string GSMTitleImg = "http://m.gossipspecials.com/img/GossipSpecialsBadaboomSml.jpeg";
        public static string GDTitleImg = "http://www.gossipdeals.com/img/GossipDealsLogo.jpg";
        public static string GDMTitleImg = "http://m.gossipdeals.com/img/GossipDealsLogoSml.jpg";
        public static string GOTitleImg = "http://www.gossipoffers.com/img/GossipOffersLogo.jpeg";
        public static string GOMTitleImg = "http://m.gossipoffers.com/img/GossipOffersLogoSml.jpeg";



        public static string leftMenuDiv = "leftmenu";

        public static Color GSleftMenuBColor = Color.FromArgb(1,153, 204, 223);
        public static Color GSleftMenuFColor = Color.FromArgb(1, 0, 0, 238);

        public static Color GDleftMenuBColor = Color.FromArgb(1, 165, 229, 126);
        public static Color GDleftMenuFColor = Color.FromArgb(1, 73, 130, 39);

        public static Color GOleftMenuBColor = Color.FromArgb(1, 183, 0, 2);
        public static Color GOleftMenuFColor = Color.FromArgb(1, 255, 255, 255);

    }
}
