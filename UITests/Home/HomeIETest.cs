﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;


namespace UITests
{
    [TestClass]
    public class HomeIETest
    {
        #region "HOME PAGES LOAD"

        [TestMethod]
        public void TestGSHomeLoad()
        {
            Assert.IsTrue(IEGossipTests.TestLoad(GossipConstants.GSWebHost));
        }

        [TestMethod]
        public void TestGSMHomeLoad()
        {
            Assert.IsTrue(IEGossipTests.TestLoad(GossipConstants.GSMobileWebHost));
        }

        [TestMethod]
        public void TestGDHomeLoad()
        {
            Assert.IsTrue(IEGossipTests.TestLoad(GossipConstants.GDWebHost));
        }

        [TestMethod]
        public void TestGDMHomeLoad()
        {
            Assert.IsTrue(IEGossipTests.TestLoad( GossipConstants.GDMobileWebHost));
        }

        [TestMethod]
        public void TestGOHomeLoad()
        {
            Assert.IsTrue(IEGossipTests.TestLoad(GossipConstants.GOWebHost));
        }

        [TestMethod]
        public void TestGOMHomeLoad()
        {
            Assert.IsTrue(IEGossipTests.TestLoad(GossipConstants.GOMobileWebHost));
        }
        #endregion

        #region "HOME PAGES TITLES ARE OK"

        [TestMethod]
        public void TestGSHomeTitleLoad()
        {
            Assert.IsTrue(IEGossipTests.TestTitleLoad(GossipConstants.GSWebHost, GossipConstants.GSWebHostTitle));
        }

        [TestMethod]
        public void TestGSMHomeTitleLoad()
        {
            Assert.IsTrue(IEGossipTests.TestTitleLoad(GossipConstants.GSMobileWebHost, GossipConstants.GSMobileWebHostTitle));
        }

        [TestMethod]
        public void TestGDHomeTitleLoad()
        {
            Assert.IsTrue(IEGossipTests.TestTitleLoad(GossipConstants.GDWebHost, GossipConstants.GDWebHostTitle));
        }

        [TestMethod]
        public void TestGDMHomeTitleLoad()
        {
            Assert.IsTrue(IEGossipTests.TestTitleLoad(GossipConstants.GDMobileWebHost, GossipConstants.GDMobileWebHostTitle));
        }

        [TestMethod]
        public void TestGOHomeTitleLoad()
        {
            Assert.IsTrue(IEGossipTests.TestTitleLoad(GossipConstants.GOWebHost, GossipConstants.GOWebHostTitle));
        }

        [TestMethod]
        public void TestGOMHomeTitleLoad()
        {
            Assert.IsTrue(IEGossipTests.TestTitleLoad(GossipConstants.GOMobileWebHost, GossipConstants.GOMobileWebHostTitle));
        }

        #endregion

        #region "HOME PAGES HEADER TEXT ARE OK"

        [TestMethod]
        public void TestGSHomeHeaderTextLoad()
        {
            Assert.IsTrue(IEGossipTests.TestHeaderTextLoad(GossipConstants.GSWebHost, GossipLayout.GSTitleImg));
        }

        [TestMethod]
        public void TestGSMHomeHeaderTextLoad()
        {
            Assert.IsTrue(IEGossipTests.TestHeaderTextLoad(GossipConstants.GSMobileWebHost, GossipLayout.GSMTitleImg));
        }

        [TestMethod]
        public void TestGDHomeHeaderTextLoad()
        {
            Assert.IsTrue(IEGossipTests.TestHeaderTextLoad(GossipConstants.GDWebHost, GossipLayout.GDTitleImg));
        }

        [TestMethod]
        public void TestGDMHomeHeaderTextLoad()
        {
            Assert.IsTrue(IEGossipTests.TestHeaderTextLoad(GossipConstants.GDMobileWebHost, GossipLayout.GDMTitleImg));
        }

        [TestMethod]
        public void TestGOHomeHeaderTextLoad()
        {
            Assert.IsTrue(IEGossipTests.TestHeaderTextLoad(GossipConstants.GOWebHost, GossipLayout.GOTitleImg));
        }

        [TestMethod]
        public void TestGOMHomeHeaderTextLoad()
        {
            Assert.IsTrue(IEGossipTests.TestHeaderTextLoad(GossipConstants.GOMobileWebHost, GossipLayout.GOMTitleImg));
        }

        #endregion

        #region "HOME PAGES MENU COLORS ARE OK"

        // Todo: correct the style left menu color for gs -- css commented
        //[TestMethod]
        //public void TestGSHomeMenuColorsLoad()
        //{
        //   string message;
        //    Assert.IsTrue(IEGossipTests.TestMenuColorsLoad(GossipConstants.GSWebHost, GossipLayout.GSleftMenuFColor, GossipLayout.GSleftMenuBColor, out message), message);
        //}

        //[TestMethod]
        //public void TestGSMHomeMenuColorsLoad()
        //{
        //    string message;
        //     Assert.IsTrue(IEGossipTests.TestMenuColorsLoad(GossipConstants.GSMobileWebHost, GossipLayout.GSleftMenuFColor, GossipLayout.GSleftMenuBColor, out message), message);
        //}

        [TestMethod]
        public void TestGDHomeMenuColorsLoad()
        {
            string message;
            Assert.IsTrue(IEGossipTests.TestMenuColorsLoad(GossipConstants.GDWebHost, GossipLayout.GDleftMenuFColor, GossipLayout.GDleftMenuBColor, out message), message);
        }

        //[TestMethod]
        //public void TestGDMHomeMenuColorsLoad()
        //{
        //     string message;
        //     Assert.IsTrue(IEGossipTests.TestMenuColorsLoad(GossipConstants.GDMobileWebHost, GossipLayout.GDleftMenuFColor, GossipLayout.GDleftMenuBColor, out message), message);
        //  }

        [TestMethod]
        public void TestGOHomeMenuColorsLoad()
       {
            string message;
            Assert.IsTrue(IEGossipTests.TestMenuColorsLoad(GossipConstants.GOWebHost, GossipLayout.GOleftMenuFColor, GossipLayout.GOleftMenuBColor, out message), message);
        }

        //[TestMethod]
        //public void TestGOMHomeMenuColorsLoad()
        //{
        //    string message;
        //    Assert.IsTrue(IEGossipTests.TestMenuColorsLoad(GossipConstants.GOMobileWebHost, GossipLayout.GOleftMenuFColor, GossipLayout.GOleftMenuBColor, out message), message);
        //}

        #endregion

    }
}