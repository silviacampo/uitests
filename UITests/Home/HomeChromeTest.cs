﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;


namespace UITests
{
    [TestClass]
    public class HomeChromeTest
    {
        #region "HOME PAGES LOAD"

        [TestMethod]
        public void TestGSHomeLoad()
        {
            Assert.IsTrue(ChromeGossipTests.TestLoad(GossipConstants.GSWebHost));
        }

        [TestMethod]
        public void TestGSMHomeLoad()
        {
            Assert.IsTrue(ChromeGossipTests.TestLoad(GossipConstants.GSMobileWebHost));
        }

        [TestMethod]
        public void TestGDHomeLoad()
        {
            Assert.IsTrue(ChromeGossipTests.TestLoad(GossipConstants.GDWebHost));
        }

        [TestMethod]
        public void TestGDMHomeLoad()
        {
            Assert.IsTrue(ChromeGossipTests.TestLoad( GossipConstants.GDMobileWebHost));
        }

        [TestMethod]
        public void TestGOHomeLoad()
        {
            Assert.IsTrue(ChromeGossipTests.TestLoad(GossipConstants.GOWebHost));
        }

        [TestMethod]
        public void TestGOMHomeLoad()
        {
            Assert.IsTrue(ChromeGossipTests.TestLoad(GossipConstants.GOMobileWebHost));
        }
        #endregion

        #region "HOME PAGES TITLES ARE OK"

        [TestMethod]
        public void TestGSHomeTitleLoad()
        {
            Assert.IsTrue(ChromeGossipTests.TestTitleLoad(GossipConstants.GSWebHost, GossipConstants.GSWebHostTitle));
        }

        [TestMethod]
        public void TestGSMHomeTitleLoad()
        {
            Assert.IsTrue(ChromeGossipTests.TestTitleLoad(GossipConstants.GSMobileWebHost, GossipConstants.GSMobileWebHostTitle));
        }

        [TestMethod]
        public void TestGDHomeTitleLoad()
        {
            Assert.IsTrue(ChromeGossipTests.TestTitleLoad(GossipConstants.GDWebHost, GossipConstants.GDWebHostTitle));
        }

        [TestMethod]
        public void TestGDMHomeTitleLoad()
        {
            Assert.IsTrue(ChromeGossipTests.TestTitleLoad(GossipConstants.GDMobileWebHost, GossipConstants.GDMobileWebHostTitle));
        }

        [TestMethod]
        public void TestGOHomeTitleLoad()
        {
            Assert.IsTrue(ChromeGossipTests.TestTitleLoad(GossipConstants.GOWebHost, GossipConstants.GOWebHostTitle));
        }

        [TestMethod]
        public void TestGOMHomeTitleLoad()
        {
            Assert.IsTrue(ChromeGossipTests.TestTitleLoad(GossipConstants.GOMobileWebHost, GossipConstants.GOMobileWebHostTitle));
        }

        #endregion

        #region "HOME PAGES HEADER TEXT ARE OK"

        [TestMethod]
        public void TestGSHomeHeaderTextLoad()
        {
            Assert.IsTrue(ChromeGossipTests.TestHeaderTextLoad(GossipConstants.GSWebHost, GossipLayout.GSTitleImg));
        }

        [TestMethod]
        public void TestGSMHomeHeaderTextLoad()
        {
            Assert.IsTrue(ChromeGossipTests.TestHeaderTextLoad(GossipConstants.GSMobileWebHost, GossipLayout.GSMTitleImg));
        }

        [TestMethod]
        public void TestGDHomeHeaderTextLoad()
        {
            Assert.IsTrue(ChromeGossipTests.TestHeaderTextLoad(GossipConstants.GDWebHost, GossipLayout.GDTitleImg));
        }

        [TestMethod]
        public void TestGDMHomeHeaderTextLoad()
        {
            Assert.IsTrue(ChromeGossipTests.TestHeaderTextLoad(GossipConstants.GDMobileWebHost, GossipLayout.GDMTitleImg));
        }

        [TestMethod]
        public void TestGOHomeHeaderTextLoad()
        {
            Assert.IsTrue(ChromeGossipTests.TestHeaderTextLoad(GossipConstants.GOWebHost, GossipLayout.GOTitleImg));
        }

        [TestMethod]
        public void TestGOMHomeHeaderTextLoad()
        {
            Assert.IsTrue(ChromeGossipTests.TestHeaderTextLoad(GossipConstants.GOMobileWebHost, GossipLayout.GOMTitleImg));
        }

        #endregion

        #region "HOME PAGES MENU COLORS ARE OK"

        [TestMethod]
        public void TestGSHomeMenuColorsLoad()
        {
           string message;
            Assert.IsTrue(ChromeGossipTests.TestMenuColorsLoad(GossipConstants.GSWebHost, GossipLayout.GSleftMenuFColor, GossipLayout.GSleftMenuBColor, out message), message);
        }

        //[TestMethod]
        //public void TestGSMHomeMenuColorsLoad()
        //{
        //    string message;
       //     Assert.IsTrue(ChromeGossipTests.TestMenuColorsLoad(GossipConstants.GSMobileWebHost, GossipLayout.GSleftMenuFColor, GossipLayout.GSleftMenuBColor, out message), message);
        //}

        [TestMethod]
        public void TestGDHomeMenuColorsLoad()
        {
            string message;
            Assert.IsTrue(ChromeGossipTests.TestMenuColorsLoad(GossipConstants.GDWebHost, GossipLayout.GDleftMenuFColor, GossipLayout.GDleftMenuBColor, out message), message);
        }

        //[TestMethod]
        //public void TestGDMHomeMenuColorsLoad()
        //{
       //     string message;
       //     Assert.IsTrue(ChromeGossipTests.TestMenuColorsLoad(GossipConstants.GDMobileWebHost, GossipLayout.GDleftMenuFColor, GossipLayout.GDleftMenuBColor, out message), message);
      //  }

        [TestMethod]
        public void TestGOHomeMenuColorsLoad()
       {
            string message;
            Assert.IsTrue(ChromeGossipTests.TestMenuColorsLoad(GossipConstants.GOWebHost, GossipLayout.GOleftMenuFColor, GossipLayout.GOleftMenuBColor, out message), message);
        }

        //[TestMethod]
        //public void TestGOMHomeMenuColorsLoad()
        //{
        //    string message;
        //    Assert.IsTrue(ChromeGossipTests.TestMenuColorsLoad(GossipConstants.GOMobileWebHost, GossipLayout.GOleftMenuFColor, GossipLayout.GOleftMenuBColor, out message), message);
        //}

        #endregion
    
    }
}