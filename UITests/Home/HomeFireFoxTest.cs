﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;


namespace UITests
{
    [TestClass]
    public class HomeFireFoxTest
    {
        #region "HOME PAGES LOAD"

        [TestMethod]
        public void TestGSHomeLoad()
        {
            Assert.IsTrue(FireFoxGossipTests.TestLoad(GossipConstants.GSWebHost));
        }

        [TestMethod]
        public void TestGSMHomeLoad()
        {
            Assert.IsTrue(FireFoxGossipTests.TestLoad(GossipConstants.GSMobileWebHost));
        }

        [TestMethod]
        public void TestGDHomeLoad()
        {
            Assert.IsTrue(FireFoxGossipTests.TestLoad(GossipConstants.GDWebHost));
        }

        [TestMethod]
        public void TestGDMHomeLoad()
        {
            Assert.IsTrue(FireFoxGossipTests.TestLoad(GossipConstants.GDMobileWebHost));
        }

        [TestMethod]
        public void TestGOHomeLoad()
        {
           Assert.IsTrue(FireFoxGossipTests.TestLoad(GossipConstants.GOWebHost));
        }

        [TestMethod]
        public void TestGOMHomeLoad()
        {
            Assert.IsTrue(FireFoxGossipTests.TestLoad(GossipConstants.GOMobileWebHost));
        }
        #endregion

        #region "HOME PAGES TITLES ARE OK"

        [TestMethod]
        public void TestGSHomeTitleLoad()
        {
            Assert.IsTrue(FireFoxGossipTests.TestTitleLoad(GossipConstants.GSWebHost, GossipConstants.GSWebHostTitle));
        }

        [TestMethod]
        public void TestGSMHomeTitleLoad()
        {
            Assert.IsTrue(FireFoxGossipTests.TestTitleLoad(GossipConstants.GSMobileWebHost, GossipConstants.GSMobileWebHostTitle));
        }

        [TestMethod]
        public void TestGDHomeTitleLoad()
        {
            Assert.IsTrue(FireFoxGossipTests.TestTitleLoad(GossipConstants.GDWebHost, GossipConstants.GDWebHostTitle));
        }

        [TestMethod]
        public void TestGDMHomeTitleLoad()
        {
            Assert.IsTrue(FireFoxGossipTests.TestTitleLoad(GossipConstants.GDMobileWebHost, GossipConstants.GDMobileWebHostTitle));
        }

        [TestMethod]
        public void TestGOHomeTitleLoad()
        {
            Assert.IsTrue(FireFoxGossipTests.TestTitleLoad(GossipConstants.GOWebHost, GossipConstants.GOWebHostTitle));
        }

        [TestMethod]
        public void TestGOMHomeTitleLoad()
        {
            Assert.IsTrue(FireFoxGossipTests.TestTitleLoad(GossipConstants.GOMobileWebHost, GossipConstants.GOMobileWebHostTitle));
        }

        #endregion

        #region "HOME PAGES HEADER TEXT ARE OK"

        [TestMethod]
        public void TestGSHomeHeaderTextLoad()
        {
            Assert.IsTrue(FireFoxGossipTests.TestHeaderTextLoad(GossipConstants.GSWebHost, GossipLayout.GSTitleImg));
        }

        [TestMethod]
        public void TestGSMHomeHeaderTextLoad()
        {
            Assert.IsTrue(FireFoxGossipTests.TestHeaderTextLoad( GossipConstants.GSMobileWebHost, GossipLayout.GSMTitleImg));
        }

        [TestMethod]
        public void TestGDHomeHeaderTextLoad()
        {
            Assert.IsTrue(FireFoxGossipTests.TestHeaderTextLoad( GossipConstants.GDWebHost, GossipLayout.GDTitleImg));
        }

        [TestMethod]
        public void TestGDMHomeHeaderTextLoad()
        {
            Assert.IsTrue(FireFoxGossipTests.TestHeaderTextLoad( GossipConstants.GDMobileWebHost, GossipLayout.GDMTitleImg));
        }

        [TestMethod]
        public void TestGOHomeHeaderTextLoad()
        {
            Assert.IsTrue(FireFoxGossipTests.TestHeaderTextLoad(GossipConstants.GOWebHost, GossipLayout.GOTitleImg));
        }

        [TestMethod]
        public void TestGOMHomeHeaderTextLoad()
        {
            Assert.IsTrue(FireFoxGossipTests.TestHeaderTextLoad(GossipConstants.GOMobileWebHost, GossipLayout.GOMTitleImg));
        }

        #endregion

        #region "HOME PAGES MENU COLORS ARE OK"

        [TestMethod]
        public void TestGSHomeMenuColorsLoad()
        {
            string message;
            Assert.IsTrue(FireFoxGossipTests.TestMenuColorsLoad(GossipConstants.GSWebHost, GossipLayout.GSleftMenuFColor, GossipLayout.GSleftMenuBColor, out message), message);
        }

        //[TestMethod]
        //public void TestGSMHomeMenuColorsLoad()
        //{
        //    string message;
        //     Assert.IsTrue(FireFoxGossipTests.TestMenuColorsLoad(GossipConstants.GSMobileWebHost, GossipLayout.GSleftMenuFColor, GossipLayout.GSleftMenuBColor, out message), message);
        //}

        [TestMethod]
        public void TestGDHomeMenuColorsLoad()
        {
            string message;
            Assert.IsTrue(FireFoxGossipTests.TestMenuColorsLoad(GossipConstants.GDWebHost, GossipLayout.GDleftMenuFColor, GossipLayout.GDleftMenuBColor, out message), message);
        }

        //[TestMethod]
        //public void TestGDMHomeMenuColorsLoad()
        //{
        //     string message;
        //     Assert.IsTrue(FireFoxGossipTests.TestMenuColorsLoad(GossipConstants.GDMobileWebHost, GossipLayout.GDleftMenuFColor, GossipLayout.GDleftMenuBColor, out message), message);
        //  }

        [TestMethod]
        public void TestGOHomeMenuColorsLoad()
        {
            string message;
            Assert.IsTrue(FireFoxGossipTests.TestMenuColorsLoad(GossipConstants.GOWebHost, GossipLayout.GOleftMenuFColor, GossipLayout.GOleftMenuBColor, out message), message);
        }

        //[TestMethod]
        //public void TestGOMHomeMenuColorsLoad()
        //{
        //    string message;
        //    Assert.IsTrue(FireFoxGossipTests.TestMenuColorsLoad(GossipConstants.GOMobileWebHost, GossipLayout.GOleftMenuFColor, GossipLayout.GOleftMenuBColor, out message), message);
        //}

        #endregion
    }
}