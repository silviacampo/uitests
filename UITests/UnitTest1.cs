﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;

namespace UITests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
        }
        string url = "http://www.gossipspecials.com/";

        [TestMethod]
        public void TestMethodChrome()
        {
            ChromeOptions options = new ChromeOptions();
            options.BinaryLocation = @"C:\Program Files (x86)\Google\Chrome\Application\chrome.exe";
            using (IWebDriver wdriver = new ChromeDriver(options))
            {
                wdriver.Navigate().GoToUrl(url);
                Assert.AreEqual(wdriver.Url, url);
                wdriver.Quit();
            }

        }

        [TestMethod]
        public void TestMethodFirefox()
        {
            FirefoxOptions options = new FirefoxOptions();
            options.BrowserExecutableLocation = @"C:\Program Files (x86)\Mozilla Firefox\firefox.exe";
            using (IWebDriver wdriver = new FirefoxDriver(options))
            {
                wdriver.Navigate().GoToUrl(url);
                // wdriver.Manage().Window.Maximize();
                // wdriver.FindElement(By.Id("lnklogin")).Click();

                Assert.AreEqual(wdriver.Url, url);
                wdriver.Quit();
            }

        }

        [TestMethod]
        public void TestMethodIE()
        {
            InternetExplorerOptions options = new InternetExplorerOptions();
            //options.IntroduceInstabilityByIgnoringProtectedModeSettings = true;
            
            using (IWebDriver wdriver = new InternetExplorerDriver(options))
            {
                wdriver.Navigate().GoToUrl(url);
                wdriver.Manage().Window.Maximize();
                // wdriver.FindElement(By.Id("lnklogin")).Click();

                Assert.AreEqual(wdriver.Url, url);
                wdriver.Quit();
            }

        }
    }
}
