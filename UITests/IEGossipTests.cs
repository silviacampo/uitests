﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OpenQA.Selenium.Chrome;
using System.Drawing;
using OpenQA.Selenium;
using OpenQA.Selenium.IE;

namespace UITests
{
    class IEGossipTests
    {
        public static bool TestLoad(string webPage)
        {
            bool result = false;
            using (IWebDriver browser = new InternetExplorerDriver())
            {
                result = GossipTests.TestLoad(browser, webPage);
                browser.Quit();
            }
            return result;
        }

        public static bool TestTitleLoad(string webPage, string webPageTitle)
        {
            bool result = false;
            using (IWebDriver browser = new InternetExplorerDriver())
            {
                result = GossipTests.TestTitleLoad(browser, webPage, webPageTitle);
                browser.Quit();
            }
            return result;
        }

        public static bool TestHeaderTextLoad(string webPage, string webPageTitleImage)
        {
            bool result = false;
            using (IWebDriver browser = new InternetExplorerDriver())
            {
                result = GossipTests.TestHeaderTextLoad(browser, webPage, webPageTitleImage);
                browser.Quit();
            }
            return result;
        }

        public static bool TestMenuColorsLoad(string webPage, Color color, Color bgcolor, out string message)
        {
            bool result = false;
            using (IWebDriver browser = new InternetExplorerDriver())
            {
                result = GossipTests.TestMenuColorsLoad(browser, webPage, color, bgcolor, out message);
                browser.Quit();
            }
            return result;
        }



    }
}
